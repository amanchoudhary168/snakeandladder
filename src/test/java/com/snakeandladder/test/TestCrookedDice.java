package com.snakeandladder.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sample.snakeladder.entities.CrookedDice;
import com.sample.snakeladder.entities.Dice;
import com.sample.snakeladder.entities.NormalDice;

public class TestCrookedDice {
	
	@Test
	public void testCrookedDice() {
		int faces = 7;
		Dice dice = new CrookedDice(faces);
		
		for(int i=0;i<100000;i++) {
			int val = dice.rollDice();
			//System.out.println(val);
			assertTrue(val%2==0);
			assertTrue(val>=1);
			assertTrue(val<=faces);
			
		}
			
			
	}

}
