package com.snakeandladder.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sample.snakeladder.entities.Dice;
import com.sample.snakeladder.entities.Game;
import com.sample.snakeladder.entities.Ladder;
import com.sample.snakeladder.entities.NormalDice;
import com.sample.snakeladder.entities.Player;
import com.sample.snakeladder.entities.Snake;
import com.sample.snakeladder.entities.Game.Status;
import com.sample.snakeladder.services.GameService;
import com.sample.snakeladder.services.implementations.GameServiceImpl;

public class TestGameService {
	
	
	@Test
	public void testGameSetup() {
		Player player = new Player(1, "Aman");
		GameService gameService = new GameServiceImpl();
		
		Snake snake = new Snake(14, 7);
		
		List<Snake> snakes = new ArrayList<>();
		snakes.add(snake);
		
		List<Ladder> ladders = new ArrayList<>();
		ladders.add(new Ladder(10, 20));
		
		Dice dice = new NormalDice(6);
		
		//Game Setup 
		Game singlePlayerGame  = gameService.setupSignlePlayerGame(player, snakes, ladders, dice);
		assertEquals(Status.STOPPED, singlePlayerGame.getCurrentStatus());
		assertEquals(singlePlayerGame.getBoard().getBoardSize(),100);
		assertEquals(singlePlayerGame.getBoard().getSnakeList().size(),1);
	}
	
	@Test
	public void testGamePlay() {
		Player player = new Player(1, "Aman");
		GameService gameService = new GameServiceImpl();
		
		Snake snake = new Snake(14, 7);
		
		List<Snake> snakes = new ArrayList<>();
		snakes.add(snake);
		
		List<Ladder> ladders = new ArrayList<>();
		ladders.add(new Ladder(10, 20));
		
		Dice dice = new NormalDice(6);
		
		//Game Setup 
		Game singlePlayerGame  = gameService.setupSignlePlayerGame(player, snakes, ladders, dice);
		Player winner = gameService.startGame(singlePlayerGame);
		assertTrue(winner!= null);
		//assertEquals(Status.STOPPED, singlePlayerGame.getCurrentStatus());
		//assertEquals(singlePlayerGame.getBoard().getBoardSize(),100);
		//assertEquals(singlePlayerGame.getBoard().getSnakeList().size(),1);
	}
	

}
