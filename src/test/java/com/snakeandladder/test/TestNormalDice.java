package com.snakeandladder.test;


import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sample.snakeladder.entities.Dice;
import com.sample.snakeladder.entities.NormalDice;

public class TestNormalDice {
	
	
	
	@Test
	public void testRollTheDiceFunction() {
		int faces = 6;
		Dice dice = new NormalDice(faces);
		
		for(int i=0;i<10000;i++) {
			int val = dice.rollDice();
			System.out.println(val);
			assertTrue(val<=faces);
			assertTrue(val>=1);
		}
			
			
		
	}

}
