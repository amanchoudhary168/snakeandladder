package com.sample.snakeladder;

import java.util.ArrayList;
import java.util.List;

import com.sample.snakeladder.entities.Dice;
import com.sample.snakeladder.entities.Game;
import com.sample.snakeladder.entities.Ladder;
import com.sample.snakeladder.entities.NormalDice;
import com.sample.snakeladder.entities.Player;
import com.sample.snakeladder.entities.Snake;
import com.sample.snakeladder.services.GameService;
import com.sample.snakeladder.services.implementations.GameServiceImpl;

public class ApplicationRunner{
	
	public static void main(String[] args) {
		Player player = new Player(1, "Aman");
		GameService gameService = new GameServiceImpl();
		
		Snake snake = new Snake(14, 7);
		
		List<Snake> snakes = new ArrayList<>();
		snakes.add(snake);
		
		List<Ladder> ladders = new ArrayList<>();
		ladders.add(new Ladder(10, 20));
		
		Dice dice = new NormalDice(6);
		
		//Game Setup 
		Game game = gameService.setupSignlePlayerGame(player, snakes, ladders, dice);
		Player winner = gameService.startGame(game);
		System.out.println("The winner is "+winner.getPlayerName());
		
	}
}
