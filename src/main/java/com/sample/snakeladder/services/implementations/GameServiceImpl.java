package com.sample.snakeladder.services.implementations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import com.sample.snakeladder.entities.Board;
import com.sample.snakeladder.entities.Dice;
import com.sample.snakeladder.entities.Game;
import com.sample.snakeladder.entities.Game.Status;
import com.sample.snakeladder.entities.Ladder;
import com.sample.snakeladder.entities.Player;
import com.sample.snakeladder.entities.Snake;
import com.sample.snakeladder.services.GameService;

public class GameServiceImpl implements GameService {
	private static AtomicInteger boardId = new AtomicInteger(0);
	private static AtomicInteger systemPlayerId = new AtomicInteger(0);
	private static final String SYSTEM_PLAYER_NAME = "SYSTEM" ;
	private final int BOARD_SIZE = 100;


	@Override
	public Game setupSignlePlayerGame(Player player,List<Snake> snakeList,List<Ladder> ladderList,Dice dice) {

		Player system = new Player(systemPlayerId.getAndIncrement(),SYSTEM_PLAYER_NAME);

		List<Player> playerList = new ArrayList<>();
		playerList.add(player);
		playerList.add(system);

		HashMap<Player,Integer> initalPos = new HashMap<>();
		initalPos.put(player,0);
		initalPos.put(system,0);


		Board boardSinglePlayer = new Board(boardId.getAndIncrement(),BOARD_SIZE,snakeList,ladderList);
		Game gameSinglePLayer = new Game();
		gameSinglePLayer.setBoard(boardSinglePlayer);
		gameSinglePLayer.setPlayerList(playerList);
		gameSinglePLayer.setDice(dice);
		gameSinglePLayer.setPlayerPosMap(initalPos);


		return gameSinglePLayer;
	}

	@Override
	public Game setupMultiPlayerGame(List<Player> players, List<Snake> snakeList, List<Ladder> ladderList,Dice dice) {

		HashMap<Player,Integer> initalPos = new HashMap<>();
		for(Player player : players) {
			initalPos.put(player, 0);
		}

		Board boardSinglePlayer = new Board(boardId.getAndIncrement(),BOARD_SIZE,snakeList,ladderList);
		Game gameMutliPLayer = new Game();
		gameMutliPLayer.setBoard(boardSinglePlayer);
		gameMutliPLayer.setPlayerList(players);
		gameMutliPLayer.setDice(dice);
		gameMutliPLayer.setPlayerPosMap(initalPos);

		return gameMutliPLayer;
	}

	@Override
	public Player startGame(Game game) {
		
		Player winner = null;
		Board board = game.getBoard();
		List<Ladder> ladders = board.getLaddersList();
		List<Snake> snakes = board.getSnakeList();
		Dice dice = game.getDice();
		
		game.setCurrentStatus(Status.STARTED);

		Queue<Player> playQueue = new LinkedList<>();
		playQueue.addAll(game.getPlayerList());

		while(game.getPlayerList().size() == playQueue.size()) {

			Player currentPlayer = playQueue.poll();
			int playerCurrentPos = game.getPlayerPosMap().get(currentPlayer);
			int playerFinalPos = playerCurrentPos;
			int temp = dice.rollDice();
			Ladder ladder = null;
			Snake snake = null;

			if(playerCurrentPos+temp <=game.getBoard().getBoardSize()) {

				playerFinalPos += temp;
				ladder = isLadderPresent(playerCurrentPos+temp,ladders);
				snake = isSnakePresent(playerCurrentPos+temp, snakes);
				if(ladder != null) {
					playerFinalPos = ladder.getEnd();
				}else if(snake!= null) {
					playerFinalPos = snake.getEnd();
				}
				if(playerFinalPos == board.getBoardSize()) {
					winner = currentPlayer;
				}
				game.getPlayerPosMap().put(currentPlayer, playerFinalPos);		
			}
			
			if(winner == null) {
				playQueue.add(currentPlayer);	
			}
			logTurn(currentPlayer, snake, ladder, temp,playerCurrentPos,playerFinalPos);
		}
		game.setCurrentStatus(Status.COMPLETED);
		return winner;
	}

	@Override
	public Status getGameStatus(Game game) {
		return game.getCurrentStatus();
	}


	private Ladder isLadderPresent(long pos,List<Ladder> ladderList) {
		Ladder lad = null;
		for(Ladder ladder : ladderList ) {
			if(ladder.getStart() == pos)
				lad = ladder;
		}
		return lad;

	}

	private Snake isSnakePresent(long pos,List<Snake> SnakeList) {
		Snake s = null;
		for(Snake snake : SnakeList ) {
			if(snake.getStart() == pos)
				s = snake;
		}
		return s;

	}

	private void logTurn(Player player,Snake snake,Ladder ladder,int rolledNumber,int currentPos,int finalPos) {
		StringBuffer msg = new StringBuffer();
		msg.append("Player_Name=").append(player.getPlayerName()).
		append(",Current_Pos=").append(currentPos).
		append(",Rolled_Number=").append(rolledNumber);

		if(ladder!= null)
			msg.append(",Found a ladder,Start=").append(ladder.getStart()).append(",End=").append(ladder.getEnd());
		if(snake!= null)
			msg.append(",Found a snake,Start=").append(snake.getStart()).append(",End=").append(snake.getEnd());
		msg.append(",Final_Pos=").append(finalPos);

		System.out.println(msg);

	}



}
