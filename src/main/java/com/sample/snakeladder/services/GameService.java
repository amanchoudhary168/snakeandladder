package com.sample.snakeladder.services;

import java.util.List;

import com.sample.snakeladder.entities.Board;
import com.sample.snakeladder.entities.Dice;
import com.sample.snakeladder.entities.Game;
import com.sample.snakeladder.entities.Ladder;
import com.sample.snakeladder.entities.Player;
import com.sample.snakeladder.entities.Snake;

public interface GameService {
	
	public Game setupSignlePlayerGame(Player player,List<Snake> snakeList,List<Ladder> ladderList,Dice dice);
	public Game setupMultiPlayerGame(List<Player> player,List<Snake> snakeList,List<Ladder> ladderList,Dice dice);
	public Player startGame(Game game);
	public Game.Status getGameStatus(Game game);
	
	

}
