package com.sample.snakeladder.entities;

public abstract class Dice {
	
	int faces;
	
	public Dice(int faces) {
		this.faces = faces;
	}
	
	public abstract int rollDice();

}
