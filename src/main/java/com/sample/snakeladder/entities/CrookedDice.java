package com.sample.snakeladder.entities;

public class CrookedDice extends Dice{

	public CrookedDice(int faces) {
		super(faces);
		
	}

	@Override
	public int rollDice() {
		int tempVal  = (int)(Math.random()*((faces-1)+1));
		tempVal = tempVal+1;
		
		if (tempVal%2 != 0) {
			if (tempVal == faces)
				tempVal--;
			else
				tempVal++;
		}
		return tempVal;
	}

}
