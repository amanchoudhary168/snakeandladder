package com.sample.snakeladder.entities;

import java.util.List;

public class Board {
	
	private int boardId;
	private int boardSize;
	//private List<Player> playerList;
	private List<Snake> snakeList;
	private List<Ladder> laddersList;
	
	
	
	public Board(int boardId,int boardSize,List<Snake> snakeList,List<Ladder> ladderList) {
		this.boardId = boardId;
		this.boardSize = boardSize;
		
		this.snakeList = snakeList;
		this.laddersList = ladderList;
	}



	public int getBoardId() {
		return boardId;
	}



	public int getBoardSize() {
		return boardSize;
	}


	public List<Snake> getSnakeList() {
		return snakeList;
	}



	public List<Ladder> getLaddersList() {
		return laddersList;
	}
	
	
	

}
