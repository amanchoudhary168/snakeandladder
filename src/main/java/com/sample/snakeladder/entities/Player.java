package com.sample.snakeladder.entities;

public class Player {
	
	private long playerId;
	private String playerName;
	
	
	
	public Player(long playerId, String playerName) {
		this.playerId = playerId;
		this.playerName = playerName;
	}



	public long getPlayerId() {
		return playerId;
	}



	public String getPlayerName() {
		return playerName;
	}
	
	
	
	
}
