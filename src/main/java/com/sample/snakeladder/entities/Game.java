package com.sample.snakeladder.entities;

import java.util.HashMap;
import java.util.List;

public class Game {
	
	public enum Status{
		STARTED,COMPLETED,STOPPED;
	}
	
	private Board board;
	private List<Player> playerList;
	private HashMap<Player,Integer> playerPosMap;
	private Dice dice;
	private Status currentStatus = Status.STOPPED;
	
	
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	public List<Player> getPlayerList() {
		return playerList;
	}
	public void setPlayerList(List<Player> playerList) {
		this.playerList = playerList;
	}
	public HashMap<Player, Integer> getPlayerPosMap() {
		return playerPosMap;
	}
	public void setPlayerPosMap(HashMap<Player, Integer> playerPosMap) {
		this.playerPosMap = playerPosMap;
	}
	public Dice getDice() {
		return dice;
	}
	public void setDice(Dice dice) {
		this.dice = dice;
	}
	public Status getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(Status currentStatus) {
		this.currentStatus = currentStatus;
	}
	@Override
	public String toString() {
		return "Game [board=" + board + ", playerList=" + playerList + ", playerPosMap=" + playerPosMap + ", dice="
				+ dice + ", currentStatus=" + currentStatus + "]";
	} 
	
	
	
	
	

}
