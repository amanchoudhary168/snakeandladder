package com.sample.snakeladder.entities;

public class NormalDice extends Dice{
	
	public NormalDice(int faces) {
		super(faces);
	}

	@Override
	public int rollDice() {
		int tempVal  = (int)(Math.random()*((faces-1)+1));
		return tempVal+1;
	}

}
